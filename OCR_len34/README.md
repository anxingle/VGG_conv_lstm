#  fcn全连接网络+RNN           
     1.   切片为单列 第一层全连接中neuron为1024，batch_size:3000(太大)。 170epoch错误率 0.27/0.31              
![enter description here][1]
  第一层全连接neuron为512，batch_size:2500. 170epoch 错误率0.154/0.186；230epoch错误率0.154/0.186；280epoch错误率 0.063/0.078           
                   ![enter description here][2] 
     2.    切片为2列 第一层全连接中neuron为1024，batch_size:2500。280epoch错误率0.093/0.127.       
       ![enter description here][3] 
                     第一层全连接中neuron为512，batch_size：2500. 260epoch错误率0.089/0.12.       
                       ![enter description here][4] 
     3.    切片为3列 第一层全连接中neuron为1024，batch_size:2500.     
       ![enter description here][5] 
                      第一层全连接中neuron为512，batch_size:2500.                      
                        ![enter description here][6] 
     4.    切片为3列，步长为2列（按列读取数据）。 第一层全连接中neuron为1024.batch_size:2500（较难训练，470epoch时终止）            
       ![enter description here][7] 
                                                第一层全连接中neuron为512.batch_size:2500（280epoch准确率为0）       
                                                  ![enter description here][8] 
     5.     切片为3列，步长为2列（按行读取数据）。第一层全连接中neuron为1024.（230epoch是准确率为0）                                     
       ![enter description here][9] 
                                                 第一层全连接中neuron为512.batch_size:2800 (210epoch错误率0.085/0.165)                      
                                                  ![enter description here][10] 
      

#  conv卷积+RNN            
    1.  切片为5列，步长为4列。 batch_size:2700           
   ![enter description here][11]
    2.  切片为5列，步长为3列。 batch_size:2700          
    ![enter description here][12]           
    3. 4.  no pooling  and result is not very good!     
    

  [1]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/1/1.jpg
  [2]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/1/2.jpg
  [3]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/2/1.jpg
  [4]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/2/2.jpg
  [5]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/3/1.jpg
  [6]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/3/2.jpg
  [7]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/4/1.jpg
  [8]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/4/2.jpg
  [9]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/5/1.jpg
  [10]:https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/fcn/5/2.jpg
  [11]: https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/conv/1/1.jpg
  [12]: https://raw.githubusercontent.com/anxingle/anxingle.github.io/master/public/img/experiment_report/conv/2/2.jpg
