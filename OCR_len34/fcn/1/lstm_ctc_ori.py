#!/usr/bin/env python
# coding=utf-8
import tensorflow as tf
try:
    import tensorflow.contrib.ctc as ctc
except ImportError:
    from tensorflow import nn as ctc
import numpy as np
import time
import load_data_slice as load_data
print ("Packages imported")
# config options 
train_dir       =  "/home/a/workspace/ocr_new/"
test_dir        =  "/home/a/workspace/ocr_new_test/"
nclasses        =  10
training_epochs =  2200
display_step    =  10
learning_rate   =  0.001
num_layers      =  1
dimhidden       = 100
dimoutput       = nclasses+1
print("dimoutput:   ",dimoutput)
ROW             = 80
batch_size      = 2500
validation      = 300
width           = 1
stride          = 1
nsteps          = 120 #(120-width+stride)/stride
diminput        = 512 # feed to RNN
READ_ROW        = False
slice_num       = nsteps
SEED            = 66478
Feature_MAP     = 8
channels        = 1
begin_time      = time.time()
mnist = load_data.read_data_sets(train_dir,test_dir,\
                         slice_num,width,stride,read_row=READ_ROW,\
                         one_hot=False,validation_size=validation)
read_time       = time.time() -begin_time
print("\n\n Read All images cost %.9f\n\n"%read_time)
trainimgs, trainlabels, testimgs, testlabels = mnist.train.images,\
                                               mnist.train.labels,\
                                               mnist.test.images,\
                                               mnist.test.labels
ntrain, ntest, dim \
 = trainimgs.shape[0], testimgs.shape[0], trainimgs.shape[1]
print("ntrain:  ",ntrain)
print("dim:     ",dim)
print("\n********************")
print("mnist.train.images shape :",trainimgs.shape)

def sparse_tuple_from(sequences, dtype=np.int32):
    indices = []
    values = []
    for n, seq in enumerate(sequences):
        length = len(seq[0])
        indices.extend(zip([n]*(length), xrange((length))))
        for loop in xrange(length):
            values.extend([seq[0][loop]])
    indices = np.asarray(indices, dtype=np.int64)
    values = np.asarray(values, dtype=dtype)
    shape = np.asarray([len(sequences), np.asarray(indices).max(0)[1]+1], dtype=np.int64)
    return indices, values, shape

graph = tf.Graph()
with graph.as_default():
    weights = {
        'hidden': tf.Variable(tf.random_normal([diminput, dimhidden])),
        'out': tf.Variable(tf.random_normal([dimhidden, dimoutput]))
    }
    biases = {
        'hidden': tf.Variable(tf.random_normal([dimhidden])),
        'out': tf.Variable(tf.random_normal([dimoutput]))
    }
    #Convolution figure
    conv1_w  = tf.Variable(tf.truncated_normal([3,3,1,Feature_MAP],
                          stddev=0.1,seed=SEED,dtype=tf.float32))
    conv1_b  = tf.Variable(tf.zeros([Feature_MAP],dtype=tf.float32))
    fc1_w    = tf.Variable(tf.truncated_normal([ROW//2*(width//2+1)*Feature_MAP,diminput],\
                          stddev=0.1,seed=SEED,dtype=tf.float32))
    fc1_b    = tf.Variable(tf.constant(0.1,shape=[diminput],dtype=tf.float32))
    #DNN figure
    fc_w = tf.Variable(
             tf.truncated_normal([width*ROW,diminput],
                   stddev=0.1,seed=SEED,dtype=tf.float32))
    fc_b = tf.Variable(tf.constant(0.1,shape=[diminput],dtype=tf.float32))
    #read ROW is:
    #x = tf.placeholder(tf.float32, [batch_size, nsteps, ROW,width,channels])
    #read COL is:
    x = tf.placeholder(tf.float32, [batch_size, nsteps, ROW*width*channels])
    istate = tf.placeholder(tf.float32, [batch_size, 2*dimhidden]) #state & cell => 2x n_hidden
    y = tf.sparse_placeholder(tf.int32)
    # 1d array of size [batch_size]
    # Seq len indicates the quantity of true data in the input, since when working with batches we have to pad with zeros to fit the input in a matrix
    seq_len = tf.placeholder(tf.int32, [None])
    def RNN_model(_X,batch_size, _W, _b,num_layers,_nsteps, _name):
        #XXX: 2016-12-7 20:00 
        # 1.[batchsize, nsteps, diminput] => [nsteps, batchsize, diminput]
        #_X = tf.transpose(_X, [1, 0, 2])
        # 2. Reshape input to [nsteps*batchsize, diminput]
        _X = tf.reshape(_X, [-1, diminput])
        # 3. Input layer => Hidden layer
        _H = tf.matmul(_X, _W['hidden']) + _b['hidden']
        # 4. Splite data to 'nsteps' chunks. An i-th chunck indicates i-th batch data
        _Hsplit = tf.split(0, _nsteps, _H)
        # 5. Get LSTM's final output (_O) and state (_S)
        #    Both _O and _S consist of 'batchsize' elements
        with tf.variable_scope(_name):
            lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(dimhidden, \
                                         forget_bias=1.0)
            lstm_cell = tf.nn.rnn_cell.MultiRNNCell([lstm_cell]*num_layers,\
                                         state_is_tuple=True)
            state     = lstm_cell.zero_state(batch_size,dtype=tf.float32)
            _LSTM_O, _LSTM_S = tf.nn.rnn(lstm_cell, _Hsplit, \
                                         initial_state=state)
        # 6. Output
        _O = [tf.matmul(x, _W['out']) + _b['out'] for x in _LSTM_O]
        _O = tf.pack(_O)
        return {
            'X': _X, 'H': _H, 'Hsplit': _Hsplit,
            'LSTM_O': _LSTM_O, 'LSTM_S': _LSTM_S, 'O': _O
        }
    def CONV_model(X,conv1_w,conv1_b,fc1_w,fc1_b):
        conv       = tf.nn.conv2d(X,conv1_w,strides=[1,1,1,1],padding='SAME')
        relu       = tf.nn.relu(tf.nn.bias_add(conv,conv1_b))
        pool       = tf.nn.max_pool(relu,ksize=[1,2,2,1],\
                        strides=[1,2,2,1],padding='SAME')
        pool_shape = pool.get_shape().as_list()
        print("\npool shape is :  ",pool_shape)
        reshape    = tf.reshape(pool,\
                      [pool_shape[0],pool_shape[1]*pool_shape[2]*pool_shape[3]])
        hidden     = tf.nn.relu(tf.matmul(reshape,fc1_w)+fc1_b)
        return hidden              
    def DNN_model(input_data,fc_w,fc_b):
        X_shape  = input_data.get_shape().as_list()
        feature  = tf.nn.relu(tf.matmul(input_data,fc_w) + fc_b)
        return feature   
    input = []
    for i_ in xrange(slice_num):
        #cnn_feature shape : [Batch_size , width*ROW]
        #Read ROW
        #cnn_feature = DNN_model(x[:,i_,:,:,:],fc_w,fc_b)
        #Read COl
        cnn_feature = DNN_model(x[:,i_,:],fc_w,fc_b)
        input.append(cnn_feature)
    input = tf.pack(input)
    myrnn = RNN_model(input,batch_size, weights, biases,num_layers,nsteps, 'basic')
    pred = myrnn['O']
    loss = ctc.ctc_loss(pred, y, seq_len)
    with tf.name_scope("train_loss"):
        cost = tf.reduce_mean(loss)
        tf.scalar_summary("train_loss",cost)
    optm = tf.train.AdamOptimizer(learning_rate).minimize(cost)
    decoded, log_prob = ctc.ctc_greedy_decoder(pred, seq_len)
    accr = tf.reduce_mean(tf.edit_distance(tf.cast(decoded[0], tf.int32), y))
    init  = tf.initialize_all_variables()
    print ("Network Ready!")
with tf.Session(graph=graph) as sess:
    sess.run(init)
    merged = tf.merge_all_summaries()
    summary_writer = tf.train.SummaryWriter('./logs/', graph=sess.graph)
    print ("Start optimization")
    start_time = time.time()
    saver = tf.train.Saver()
    #saver.restore(sess,'./logs/train_ctc.tfmodel-210')
    for epoch in range(training_epochs):
        avg_cost = 0.
        total_batch = int(mnist.train.num_examples/batch_size)+2
        for i in range(total_batch):
            batch_xs, batch_ys = mnist.train.next_batch(batch_size)
            #Read ROW
            #batch_xs = batch_xs.reshape((batch_size, nsteps, ROW,width,channels))
            #Read COL
            batch_xs = batch_xs.reshape((batch_size, nsteps, ROW*width*channels))
            feed_dict={x: batch_xs, y: sparse_tuple_from([[value] for value in batch_ys]),\
                                         seq_len: [nsteps for _ in xrange(batch_size)]} 
            _, batch_cost,train_loss = sess.run([optm, cost,merged], feed_dict=feed_dict)
            avg_cost += batch_cost*batch_size
            if i%5 == 0 and epoch > 0:
                summary_writer.add_summary(train_loss,(epoch-1)*total_batch+i)
        avg_cost /= len(trainimgs)
        if epoch % display_step == 0:
            elapsed_time = time.time() - start_time
            start_time = time.time()
            print("every epoch takes time: %.9f  "%elapsed_time)
            print ("Epoch: %03d/%03d cost: %.9f" % (epoch, training_epochs, avg_cost))
            train_acc = sess.run(accr, feed_dict=feed_dict)
            print ("    Training    label    error   rate:   %.3f" % (train_acc))
            batch_txs,batch_tys = mnist.test.next_batch(batch_size)
            #Read ROW
            #batch_txs = batch_txs.reshape((batch_size,nsteps,ROW,width,channels))
            #Read COL
            batch_txs = batch_txs.reshape((batch_size,nsteps,ROW*width*channels))
            feed_dict={x:batch_txs, y: sparse_tuple_from([[value] for value in batch_tys]), \
                                 seq_len: [nsteps for _ in xrange(batch_size)]}
            test_acc = sess.run(accr, feed_dict=feed_dict)
            print (" Test label error rate: %.3f" % (test_acc))
            print("                   batch_ys is :  "+str(batch_tys[0])+"  ")
            p = sess.run(decoded[0],feed_dict=feed_dict)
            print("prediction is :            ")
            print(p[1])
            saver.save(sess,'./logs/train_ctc.tfmodel',epoch)
print ("Optimization Finished.")
