#!/usr/bin/env python
# coding=utf-8
import numpy as np
import struct
import os
import cv2
import random
ROW   = 31
COL   = 120
depth = 1
def char2num(name):
    if name >= 'a' and name <= 'z':
        return ord(name) - 61
    elif name >= 'A' and name <= 'Z':
        return ord(name) - 55
    else:
        return ord(name) - 48 
def dense_to_one_hot(labels_dense, num_classes):
  """Convert class labels from scalars to one-hot vectors."""
  num_labels = labels_dense.shape[0]
  index_offset = np.arange(num_labels) * num_classes
  labels_one_hot = np.zeros((num_labels, num_classes))
  labels_one_hot.flat[[index_offset + labels_dense.ravel()]] = 1
  return labels_one_hot
#read_row: [batch_, slice_num, ROW,width]
#read_row False: [batch_, slice_num, ROW*width]
def read_images(train_dir,slice_num,width,stride,read_row):
    #print "read row is : ",read_row
    #XXX: list files in the Dir
    images = os.listdir(train_dir)
    # volume of data_set (train_dir)
    data_volume = len(images)
    # get a random list intend to shuffle the data_set 
    images_list = range(data_volume)
    random.shuffle(images_list)
    random.shuffle(images_list)
    if read_row is False:
        data  = np.zeros((data_volume,slice_num,ROW*width))
    else:
        data  = np.zeros((data_volume,slice_num,ROW,width,depth))
    label  = []
    #read every picture
    for loop in xrange(data_volume):
        image_name  =  images[images_list[loop]]
        image       =  os.path.join(train_dir,image_name)
        #img shape [ROW,COL]
        img         =  cv2.imread(image,cv2.IMREAD_GRAYSCALE)
        if read_row is False:
            img_whole = np.zeros((slice_num,ROW*width))
        else:
            img_whole = np.zeros((slice_num,ROW,width,depth))
        startP      =  0
        #XXX: read slice of picture
        for n in xrange(slice_num):
            startP       =  n*stride
            img_slice    =  img[:,startP:startP+width]
            if read_row is False:
                img_slice    = img_slice.transpose(1,0)
                img_whole[n] = img_slice.reshape((ROW*width))
            else:
                img_whole[n] =  img_slice.reshape(ROW,width,depth)
        data[loop]  =  img_whole

        image_name  =  image_name.strip()
        image_name  =  image_name.split('.')[0]
        image_name  =  image_name.split('_')

        image_label = []
        length_name = len(image_name)-2
        for i in xrange(length_name):
            image_label.append(image_name[i+1])
        label.append(image_label)
        
    label_num = np.array(label)
    return data,label_num
def read_data(train_dir,slice_num,width,stride,read_row,one_hot=False):
#def read_data(train_dir,one_hot=True,dtype=dtypes.float32,reshape=True):
    train_images,train_labels = read_images(train_dir,slice_num,width,stride,read_row)
    return train_images,train_labels